#!/bin/sh

# delete the file if it exists first
rm -f /home/recordings/"$2".mp3

# start recording
timeout $3 wget $1 --timeout=30 --tries=0 -q -O - >> /home/recordings/"$2".mp3
