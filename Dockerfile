FROM alpine:latest

# RUN apk add vlc --no-cache

# to make vlc run as root
# RUN sed -i 's/geteuid/getppid/' /usr/bin/vlc

WORKDIR "/home"

COPY recorder.sh .

COPY renamer.sh .

COPY entry.sh /entry.sh

RUN mkdir recordings

RUN chmod 755 /home/recorder.sh /entry.sh /home/renamer.sh

CMD ["/entry.sh"]



